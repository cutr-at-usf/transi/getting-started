# Welcome!

This repository keeps a list of projects compatible with Transi-Core as a data input source. 

## [Transi-Core](https://gitlab.com/cutr-at-usf/transi/core)

Cloud-native GTFS-RT/GTFS archiving system. It can be used to archive, replay, and query feeds.

## [retro-gtfs](https://github.com/CUTR-at-USF/retro-gtfs/tree/GTFS-Realtime)

Application designed to process real-time transit data into a "retrospective" or "retroactive" GTFS package